
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define BUF_SIZE 4096

int main(void){

    int ruta, in;
    char buf[BUF_SIZE];

    ruta = open("archivo.txt", O_RDONLY);

    if (ruta < 0){
        write( 1, "Error Con el archivo", 20);
    }

    while (1){
        
        in = read(ruta, buf, BUF_SIZE);
        if (in <= 0){
            break;
        }

        write(1, buf, strlen(buf));
    }
    
    close(ruta);
    
    return 0;
}
